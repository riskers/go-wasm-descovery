package main

import (
	"syscall/js"
)

func Hello(this js.Value, args []js.Value) interface{} {
	message := args[0].String()
	return "Hello " + message
}

func ShowThis(this js.Value, args []js.Value) interface{} {
	// this refer to js global context - Browser - `window`
	return this
}

func main() {
	js.Global().Set("Hello", js.FuncOf(Hello))
	js.Global().Set("ShowThis", js.FuncOf(ShowThis))

	<-make(chan bool)
}
