package main

import (
	"syscall/js"
)

func Hello(this js.Value, args []js.Value) interface{} {
	human := args[0]

	firstName := human.Get("firstName").String()
	lastName := human.Get("lastName").String()

	return map[string]interface{}{
		"message": "Hello " + firstName + " " + lastName,
		"author":  "@risker",
	}
}

func main() {
	js.Global().Set("Hello", js.FuncOf(Hello))

	<-make(chan bool)
}
