package main

import (
	"syscall/js"
)

func Hello(this js.Value, args []js.Value) interface{} {
	firstName := args[0].Index(0).String()
	lastName := args[0].Index(1).String()

	return []interface{}{
		"Hello",
		firstName,
		lastName,
		"@risker",
	}
}

func main() {
	js.Global().Set("Hello", js.FuncOf(Hello))

	<-make(chan bool)
}
