package main

import (
	"fmt"
	"syscall/js"
)

func main() {

	// 调用 JS 的 sayHello 函数
	fmt.Println(js.Global().Call("sayHello", "Bill"))

	// 得到 message JS 变量
	message := js.Global().Get("message").String()
	fmt.Println("message (before):", message)

	// 改变 message JS 变量
	js.Global().Set("message", "Hello from Go")

	// 得到 bill JS 变量
	bill := js.Global().Get("bill")
	fmt.Println("bill (before):", bill)

	// 改变 bill JS 变量
	bill.Set("firstName", "Bill")
	bill.Set("lastName", "Ballantine")

	<-make(chan bool)
}
