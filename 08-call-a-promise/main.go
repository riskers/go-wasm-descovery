package main

import (
	"fmt"
	"syscall/js"
)

func main() {

	thenFunc := func(this js.Value, args []js.Value) interface{} {
		fmt.Println("All good:", args[0].String())
		return ""
	}

	catchFunc := func(this js.Value, args []js.Value) interface{} {
		fmt.Println("All good:", args[0].Get("message"))
		return ""
	}

	// 调用 compute Promise 的时候 `true` 还是 `false` 决定了是调用 Promise 的 `then`` 还是 `catch`
	js.Global().Call("compute", false).Call("then", js.FuncOf(thenFunc)).Call("catch", js.FuncOf(catchFunc))
	js.Global().Call("compute", true).Call("then", js.FuncOf(thenFunc)).Call("catch", js.FuncOf(catchFunc))

	<-make(chan bool)
}
