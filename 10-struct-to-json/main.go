package main

import (
	"encoding/json"
	"log"
	"syscall/js"
)

// 定义 `Human` 结构
type Human struct {
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
}

// 定义全局变量 `human`
var human Human = Human{"Bob", "Gao"}

func GiveMeHumanJsonString(this js.Value, args []js.Value) interface{} {
	// `json.Marshal` 方法将 `human` 对象序列化为 JSON (其实是 []byte 类型)
	// jsonHuman 大致是这种 bytes 格式 - [123 34 102 105 114 115 116 78 97 109 101 34 58 34 66 111 98 34 44 34 108 97 115 116 78 97 109 101 34 58 34 71 97 111 34 125]
	jsonHuman, err := json.Marshal(human)

	if err != nil {
		log.Fatalf("Error occured during marshaling %s", err.Error())
	}

	// 将 []byte 转换为 string，就是 JSON string 格式了: `{"firstName": "Bob", "lastName": "Gao"}`
	return string(jsonHuman)
}

// 比上面的 `GiveMeHumanJsonString` 多了一步：
// 利用 JS 的 `JSON.parse` 方法将 JSON string 转换成 JS 对象
func GiveMeHumanJsonObject(this js.Value, args []js.Value) interface{} {
	// `json.Marshal` 方法将 `human` 对象序列化为 JSON (其实是 []byte 类型)
	// jsonHuman 大致是这种 bytes - [123 34 102 105 114 115 116 78 97 109 101 34 58 34 66 111 98 34 44 34 108 97 115 116 78 97 109 101 34 58 34 71 97 111 34 125]
	jsonHuman, err := json.Marshal(human)

	if err != nil {
		log.Fatalf("Error occured during marshaling %s", err.Error())
	}

	// 将 []byte 转换为 string，就是 JSON string 格式了: `{"firstName": "Bob", "lastName": "Gao"}`
	jsonString := string(jsonHuman)

	// 利用 JS 的 parse 方法，将 JSON string 转换成 JS 对象
	JSON := js.Global().Get("JSON")
	return JSON.Call("parse", jsonString)
}

func main() {

	js.Global().Set("GiveMeHumanJsonString", js.FuncOf(GiveMeHumanJsonString))
	js.Global().Set("GiveMeHumanJsonObject", js.FuncOf(GiveMeHumanJsonObject))

	<-make(chan bool)
}
