package main

import (
	"fmt"
	"os"
)

func main() {
	args := os.Args
	argsWithoutCaller := os.Args[1:]

	fmt.Println(args)
	fmt.Println(argsWithoutCaller)

	arg1 := args[1]
	arg2 := args[2]
	arg3 := args[3]

	fmt.Println(arg1, arg2, arg3)

	<-make(chan bool)
}
