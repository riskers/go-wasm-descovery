import fs from 'fs'
import './wasm_exec.js'

function runWasm(wasmFile, args) {
  const go = new Go()

  return new Promise((resolve, reject) => {
    WebAssembly.instantiate(wasmFile, go.importObject)
      .then(result => {
        if (args) {
          go.argv = args;
        }

        go.run(result.instance)

        resolve(resolve.instance)
      })
      .catch(error => {
        reject(error)
      })
  })
}

fs.readFile('./main.wasm', (err, wasmFile) => {
  if (err) {
    console.error(err)
    return
  }

  runWasm(wasmFile).then(wasm => {
    //
  }).catch(error => {
    console.log("error", error)
  })
})